from sys import argv

def rps_round(line, rps1, rps2):
    for i1 in range(len(rps1)):
        if not ord(rps1[i1]) - ord(line[0]):
            break

    for i2 in range(len(rps2)):
        if not ord(rps2[i2]) - ord(line[2]):
            break

    return (1,2,3)[(i1+(i2-1))%3] + (0,3,6)[i2]


with open(argv[1], "r") as fr:
    print(
        sum(
            [rps_round(x, ("A", "B", "C"), ("X", "Y", "Z")) for x in fr]))
