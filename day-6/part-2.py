from sys import argv

def has_packet(buffer, cursor, size):
    if cursor + size > len(buffer):
        return False

    if len(set(buffer[cursor:(cursor+size)])) < size:
        return has_packet(buffer, cursor+1, size)

    return True

def find_packet(buffer, cursor, size):
    if cursor + size > len(buffer):
        return None

    if len(set(buffer[cursor:(cursor+size)])) < size:
        return find_packet(buffer, cursor+1, size)

    return cursor


def find_packet_per_block(buffer, size, block, block_size):

    if has_packet(buffer[:((block+1)*block_size)], max(0, block*block_size), size):
        return find_packet(buffer, max(0, block*block_size), size)

    return find_packet_per_block(buffer, size, block+1, block_size)


with open(argv[1], "r") as fr:
    print(
        find_packet_per_block([x for x in fr][0], 14, 0, 100) + 14)
