from sys import argv

def find_max(lines, maxcal):
    if not lines:
        return maxcal

    return find_max(
            *find_next_max(lines, 0, maxcal))


def find_next_max(lines, sumcal, maxcal):
    if not lines:
        return [], sumcal if sumcal > maxcal else maxcal

    try:
        return find_next_max(lines[1:], sumcal + int(lines[0]), maxcal)
    except ValueError:
        return lines[1:], sumcal if sumcal > maxcal else maxcal


with open(argv[1], "r") as fr:
    print(
        find_max([x for x in fr], 0))
