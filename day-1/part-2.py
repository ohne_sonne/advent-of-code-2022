from sys import argv

def find_max(lines, maxcals):
    if not lines:
        return maxcals

    return find_max(
            *find_next_max(lines, 0, sorted(maxcals)))


def find_next_max(lines, sumcal, maxcals):
    if not lines:
        return [], [sumcal] + maxcals[1:] if sumcal > maxcals[0] else maxcals

    try:
        return find_next_max(lines[1:], sumcal + int(lines[0]), maxcals)
    except ValueError:
        return lines[1:], [sumcal] + maxcals[1:] if sumcal > maxcals[0] else maxcals


try:
    with open(argv[1], "r") as fr:
        print(sum(find_max([x for x in fr], [0] * int(argv[2]))))
except:
    print(f"USAGE: {argv[0]} input size")
