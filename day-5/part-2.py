from sys import argv

def str_cmp(str1, str2):
    if str1 < str2:
        return -1
    elif str1 > str2:
        return 1
    else:
        return 0


def join(glue, prefix, strlist):
    if not len(strlist):
        return prefix

    if prefix is not None:
        return join(glue, prefix + glue + strlist[0], strlist[1:])

    return join(glue, strlist[0], strlist[1:])


def unjoin(glue, strlist, string):
    if not string:
        return strlist

    if str_cmp(string[:len(glue)], glue):
        return unjoin(
                glue,
                strlist[:-1] + [(strlist[-1] if len(strlist) else "") + string[0]],
                string[1:])

    return unjoin(
            glue,
            strlist + [''],
            string[len(glue):])


def parse_sections(lines):
    return parse_state([], lines), parse_moves([], lines)


def parse_state(state, lines):
    if len(lines) < 1:
        return state

    if lines[1]:
        return parse_state(
                    parse_stacks(
                        state, lines[0], 0), lines[1:])
    else:
        return parse_stacks(state, lines[0], 0)


def parse_stacks(state, line, stack):
    if stack*4 > len(line):
        return state

    if len(state) < stack + 1:
        if not str_cmp(line[stack*4+1], " "):
            return parse_stacks(state + [[]], line, stack+1)

        return parse_stacks(state + [[line[stack*4+1]]], line, stack+1)

    if not str_cmp(line[stack*4+1], " "):
        return parse_stacks(state, line, stack + 1)

    return parse_stacks(state[:stack] + [state[stack] + [line[stack*4+1]]] + state[(stack+1):], line, stack + 1)


def parse_moves(moves, lines):
    if not lines:
        return moves

    if str_cmp(lines[0][:4], "move"):
        return parse_moves(moves, lines[1:])

    return parse_moves(moves + [build_move(*(unjoin(" ", [], lines[0])[1::2]))], lines[1:])


def build_move(times, source, destination):
    return (int(source)-1, int(destination)-1, int(times))


def operate(state, next_moves):
    if not len(next_moves):
        return state

    return operate(move_crates(state, *next_moves[0]), next_moves[1:])


def move_crates(state, source, destination, times):
    if source < destination:
        return (state[:source] +
                [state[source][times:]] +
                state[(source+1):destination] +
                [state[source][:times] + state[destination]] +
                state[(destination+1):])

    elif source > destination:
        return (state[:destination] +
                [state[source][:times] + state[destination]] +
                state[(destination+1):source] +
                [state[source][times:]] +
                state[(source+1):])

    else:
        return state


def topcrates(strlist, state):
    if state:
        return topcrates(strlist + [state[0][0]], state[1:])

    return strlist


with open(argv[1], "r") as fr:
    print(
        join("", None,
            topcrates([],
                operate(
                    *parse_sections([x[:-1] for x in fr])))))
