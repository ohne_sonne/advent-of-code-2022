# Advent of Code 2022 (🇫🇷)

L'advent of code 2022, écrit en Python, mais sans utiliser les caractères `.` et `=`.

## Code interdit

### Utiliser `eval` avec `chr`

Il est interdit d'utiliser `chr` pour générer un caractère interdit, suivi de `eval` pour exécuter la commande. Exemple :

```py
# Exemple de code interdit
from sys import argv

with open(argv[1], "r") as fr:
    print(
      sum([
        eval(f"len('{x[:-1]}'{chr(46)}strip(){chr(46)}split(' '))") for x in fr]))
```

Cet exemple utilise `chr(46)` pour générer un point, et l'utilise dans une `string` pour appeler la méthode
`strip()` et `split()`.

De la même manière, utiliser `chr(61)` pour un test d'égalité est interdit.

### Affectation en boucle

En Python, utiliser le signe `=` pour créer une variable n'est pas obligatoire.
On peut utiliser une `boucle for` , et c'est interdit :

```py
# Exemple de code interdit
for foo in ("bar",):
    pass

print(foo)
```

Cependant, il est possible d'utiliser l'index d'une boucle si le but de la boucle n'était pas
uniquement de créer la variable :

```py
# Ce code n'est pas interdit
for foo in range(10):
    if bar(foo):
        print(foo)
    else:
        break

print(foo)
```

# Advent of Code 2022 (🇬🇧)

Advent of code 2022, written in Python, but without `.` nor `=`.

## Illegal moves

### Using `eval` with `chr`

You can't use `chr` to generate an illegal character and `eval` to execute the command. Example:

```py
# This is an illegal move
from sys import argv

with open(argv[1], "r") as fr:
    print(
      sum([
        eval(f"len('{x[:-1]}'{chr(46)}strip(){chr(46)}split(' '))") for x in fr]))
```

This example use `chr(46)` to generate a period, and use this period in a string to call the
method `strip()` and `split()`.

Similarly, you cannot use `chr(61)` to test for equality.

### Loop assignment

In Python, you don't need the `=` sign to create variables. You can use a `for-loop`:

```py
# This is an illegal move
for foo in ("bar",):
    pass

print(foo)
```

Although the use of a loop index is fine, if the purpose of the loop is not just to
create the variable:

```py
# This is not illegal
for foo in (...):
    ... do something
    ... if something:
        break

print(foo)
```
