from sys import argv

def overlap(line):
    for i in range(len(line)):
        if ord(line[i])-44:
            continue

        return is_overlapping(
                set(expand(line[:i])),
                set(expand(line[(i+1):])))


def is_overlapping(set1, set2):
    return len(set1) + len(set2) > len(set((*set1, *set2)))


def expand(sections):
    for i in range(len(sections)):
        if ord(sections[i])-45:
            continue

        return range(int(sections[:i]), int(sections[(i+1):])+1)


with open(argv[1], "r") as fr:
    print(
        sum(
            [1 for x in fr if overlap(x[:-1])]))


