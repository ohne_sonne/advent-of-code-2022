from sys import argv

def priority(letters):
    return [(ord(letter) - 38) % 58 for letter in letters]


def halves(string):
    return string[:len(string)//2], string[len(string)//2:]


def duplicated(str1, str2):
    return set([x for x in str1 for y in str2 if not ord(x)-ord(y)])


with open(argv[1], "r") as fr:
    print(
        sum(
            [sum(
                priority(
                    duplicated(
                        *halves(x[:-1])))) for x in fr]))
