from sys import argv

def part2(lines):
    if lines:
        return [sum(
                    priority(
                        nplicated(
                            lines[0], *lines[1:3])))] + part2(lines[3:])

    return []

def priority(letters):
    return [(ord(letter) - 38) % 58 for letter in letters]


def nplicated(line1, *rest):
    if not len(rest):
        return line1

    return set([x for x in line1 for y in nplicated(rest[0], *rest[1:]) if not ord(x)-ord(y)])


with open(argv[1], "r") as fr:
    print(sum(part2([x[:-1] for x in fr])))

